# OnlineSparseLearning

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/OnlineSparseLearning.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/OnlineSparseLearning.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/OnlineSparseLearning.jl/-/pipelines)

Experimental package implementing [this sparse dictionary learning
algorithm](https://www.di.ens.fr/~fbach/mairal_icml09.pdf).
