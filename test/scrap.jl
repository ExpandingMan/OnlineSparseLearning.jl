using OnlineSparseLearning; const OSL = OnlineSparseLearning
using OnlineSparseLearning.Lasso
using OnlineStats
using LinearAlgebra, Random
using MLDatasets

ENV["JULIA_DEBUG"] = "OnlineSparseLearning"


scrap() = quote
    train = CIFAR10(:train)

    (X, y) = train[:]

    dl = DictLearning{Float32}(1024, 2048, λ=0.001, κ₁=0.001)

    dofit!(dl, X, 1)
end

# right now we are just fitting the red channel of cifar10

dofit!(dl::DictLearning, X, j::Integer) = fit!(dl, vec(X[:,:,1,j]))
