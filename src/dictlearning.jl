
mutable struct DictLearning{𝒯<:Real,ℱ} <: OnlineStat{AbstractVector{<:Real}}
    # parameters
    m::Int  # input dimensions
    k::Int  # encoding dimensions
    λ::Float64  # ℓ₁ regularization parameter
    κ₁::Float64
    η::Int  # batch size

    compute_sparse_coding::ℱ

    # number of observations
    t::Int

    D::Matrix{𝒯}

    αt::Vector{𝒯}

    At::Matrix{𝒯}
    Bt::Matrix{𝒯}

    # this is used to store intermediate results without re-allocating
    u::Vector{𝒯}
end

#WARN: I'm now extremely doubtful this is a reasonable initial guess

function default_init_D(::Type{𝒯}, m::Integer, k::Integer) where {𝒯}
    D₀ = Matrix{𝒯}(undef, m, k)
    for j ∈ 1:k
        dv = view(D₀, :, j) 
        randn!(dv)
        dv .= rand() .* dv ./ norm(dv)
    end
    D₀
end

function default_compute_sparse_coding(m::Integer, k::Integer;
                                       λ::Real=1.2/√(m),
                                       kw...
                                      )
    function (D, x)
        #TODO: check other arguments here
        fr = Lasso.fit(LassoModel, D, x; λ=[λ], intercept=false, standardize=false, kw...)
        coef(fr)
    end
end

function DictLearning{𝒯}(m::Integer, k::Integer;
                         λ::Real=1.2/√(m), # recommended by paper
                         η::Integer=256,  # TODO: what's API for supporting mini-batches
                         κ₁::Real=0.01,  # ℓ₂ regularization of D
                         D₀::AbstractMatrix{<:Real}=default_init_D(𝒯,m,k),
                         A₀::AbstractMatrix{<:Real}=diagm(ones(𝒯, k)),
                         B₀::AbstractMatrix{<:Real}=D₀,
                         compute_sparse_coding=default_compute_sparse_coding(m, k; λ),
                        ) where {𝒯}
    ℱ = typeof(compute_sparse_coding)
    DictLearning{𝒯,ℱ}(m, k, λ, κ₁, η,
                      compute_sparse_coding,
                      0,
                      D₀,
                      Vector{𝒯}(),
                      A₀, B₀,
                      zeros(𝒯, m),
                     )
end
DictLearning(m::Integer, k::Integer; kw...) = DictLearning{Float64}(m, k; kw...)

OnlineStatsBase.nobs(dl::DictLearning) = dl.t

#TODO: have to figure out what to put here
OnlineStatsBase.value(dl::DictLearning) = missing

#TODO: the D update can easily be multi-threaded, would need per-thread u

function updateD!(dl::DictLearning)
    D = copy(dl.D)  # need a *copy* here since this gets updated
    u = dl.u
    for j ∈ 1:dl.k
        a = dl.At[:,j]
        a[j] += dl.t*dl.κ₁
        b = view(dl.Bt, :, j)
        d = view(dl.D, :, j)
        Ajj = dl.At[j,j] + dl.t*dl.κ₁
        u .= (b - D*a)/Ajj + d
        d .= u ./ max(norm(u),1)
    end
    dl.D
end

function OnlineStatsBase._fit!(dl::DictLearning, x::AbstractVector{<:Real})
    dl.t += 1  # it's important that this goes here because it's used inregularization

    α = dl.compute_sparse_coding(dl.D, x)
    @debug("sparse encoding computed:", α, count(!iszero, α))
    dl.αt = α
    dl.At .+= α*α'
    dl.Bt .+= x*α'

    updateD!(dl)

    dl
end

#TODO: set up images for testing
