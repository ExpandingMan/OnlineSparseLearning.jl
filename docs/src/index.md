```@meta
CurrentModule = OnlineSparseLearning
```

# OnlineSparseLearning

Documentation for [OnlineSparseLearning](https://gitlab.com/ExpandingMan/OnlineSparseLearning.jl).

```@index
```

```@autodocs
Modules = [OnlineSparseLearning]
```
