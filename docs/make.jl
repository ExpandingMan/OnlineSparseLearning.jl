using OnlineSparseLearning
using Documenter

DocMeta.setdocmeta!(OnlineSparseLearning, :DocTestSetup, :(using OnlineSparseLearning); recursive=true)

makedocs(;
    modules=[OnlineSparseLearning],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/OnlineSparseLearning.jl/blob/{commit}{path}#{line}",
    sitename="OnlineSparseLearning.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/OnlineSparseLearning.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
